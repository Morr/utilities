using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if PACKAGE_PLATFORMS
using Unity.Build;
#endif

namespace Utilities
{
    #if PACKAGE_PLATFORMS
    [CreateAssetMenu(menuName = "Build/Build Menu Settings")]
    public class BuildMenuSettings : ScriptableObject
    {
        [field: SerializeField]
        public BuildConfiguration RunnableBuildConfiguration { get; private set; }
    }
    #endif
}
