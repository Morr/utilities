﻿using UnityEditor;
using UnityEngine;
#if PACKAGE_PLATFORMS
using Unity.Build;
using Unity.Build.Classic;
#if PACKAGE_ADDRESSABLES
using UnityEditor.AddressableAssets;
using UnityEditor.AddressableAssets.Settings;
#endif

namespace Utilities
{
    class EntitiesBuildMenu
    {
        [MenuItem("Tools/Build Utility/Build All")]
        private static void BatchBuild()
        {
            #if PACKAGE_ADDRESSABLES
            BuildAddressables();
            #endif
            BuildConfiguration[] buildConfigurations = GetAllBuildConfigurations();

            foreach (BuildConfiguration buildConfiguration in buildConfigurations)
            {
                buildConfiguration.Build();
            }
        }

        [MenuItem("Tools/Build Utility/Build And Run _F6")]
        private static void BuildAndRun()
        {
            #if PACKAGE_ADDRESSABLES
            BuildAddressables();
            #endif
            BuildConfiguration runnableConfiguration = GetRunnableConfiguration();

            if (runnableConfiguration != null && runnableConfiguration.CanBuild() == true)
            {
                runnableConfiguration.Build();
            }

            Run();
        }

        [MenuItem("Tools/Build Utility/Run %F6")]
        private static void Run()
        {
            BuildConfiguration runnableConfiguration = GetRunnableConfiguration();

            if (runnableConfiguration != null && runnableConfiguration.CanRun() == true)
            {
                runnableConfiguration.Run();
            }
        }

        private static BuildConfiguration GetRunnableConfiguration()
        {
            BuildMenuSettings settings = GetBuildMenuSettings();

            if (settings.RunnableBuildConfiguration == null)
            {
                Debug.Log("Runnable configuration asset not assigned");
                return null;
            }
            else
            {
                return settings.RunnableBuildConfiguration;
            }
        }

        private static BuildMenuSettings GetBuildMenuSettings()
        {
            BuildMenuSettings[] settings = ScriptableObjectHelpers.GetAllInstances<BuildMenuSettings>();

            switch (settings.Length)
            {
                case 0:
                    return ScriptableObjectHelpers.CreateScriptableObjectAsset<BuildMenuSettings>();

                case 1:
                    return settings[0];

                default:
                    Debug.Log($"Multiple build menu settings objects detected. Make sure that there is only one {typeof(BuildMenuSettings).Name} object present in assets");
                    return null;
            }
        }

        private static BuildConfiguration[] GetAllBuildConfigurations()
        {
            BuildConfiguration[] buildConfigurations = ScriptableObjectHelpers.GetAllInstances<BuildConfiguration>();
            return buildConfigurations;
        }

        #if PACKAGE_ADDRESSABLES
        private static void BuildAddressables()
        {
            AddressableAssetSettings.CleanPlayerContent(AddressableAssetSettingsDefaultObject.Settings.ActivePlayerDataBuilder);
            AddressableAssetSettings.BuildPlayerContent();
        }
        #endif
    }
}
#endif