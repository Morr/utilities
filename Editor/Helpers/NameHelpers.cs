using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utilities
{
    public static class NameHelpers
    {
        public static string GetBackingFieldName(string propertyName)
        {
            return string.Format("<{0}>k__BackingField", propertyName);
        }
    }
}

