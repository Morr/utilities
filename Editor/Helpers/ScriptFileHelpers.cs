using System.IO;
using UnityEditor;
using UnityEngine;

namespace Utilities
{
    public class ScriptFileHelpers
    {
        public static string GetScriptPath(string scriptFileName)
        {
            string[] assetsGUIDs = AssetDatabase.FindAssets(scriptFileName);

            foreach (string guid in assetsGUIDs) 
            {
                string path = AssetDatabase.GUIDToAssetPath(guid);

                if (AssetDatabase.GetMainAssetTypeAtPath(path) == typeof(MonoScript))
                {
                    if (Path.GetFileNameWithoutExtension(path).Equals(scriptFileName))
                    {
                        return path;
                    }
                }
            }

            return null;
        }

        public static string GetScriptDirectory(string scriptFileName)
        {
            string path = GetScriptPath(scriptFileName);
            return Path.GetDirectoryName(path);
        }

        public static void SaveScript(string path, string script)
        {
            TextAsset scriptAsset = AssetDatabase.LoadAssetAtPath<TextAsset>(path);

            if (scriptAsset != null)
            {
                if (script.Equals(scriptAsset.text))
                {
                    return;
                }
            }
            else
            {
                scriptAsset = new TextAsset();
                AssetDatabase.CreateAsset(scriptAsset, path);
            }

            File.WriteAllText(path, script);
            AssetDatabase.ImportAsset(path);
            AssetDatabase.SaveAssets(); 
        }
    }
}