﻿using UnityEditor;
using UnityEngine;

namespace Utilities
{
    public class ScriptableObjectHelpers
    {
        public static T CreateScriptableObjectAsset<T>(string directory = "Assets/") where T : ScriptableObject
        {
            T asset = ScriptableObject.CreateInstance<T>();
            string path = AssetDatabase.GenerateUniqueAssetPath(directory + "/" + typeof(T).Name + ".asset");
            AssetDatabase.CreateAsset(asset, path);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            return asset;
        }

        public static T[] GetAllInstances<T>() where T : ScriptableObject
        {
            string[] assetsGUIDs = AssetDatabase.FindAssets("t:" + typeof(T).Name);
            T[] instances = new T[assetsGUIDs.Length];

            for (int i = 0; i < assetsGUIDs.Length; i++) 
            {
                string path = AssetDatabase.GUIDToAssetPath(assetsGUIDs[i]);
                instances[i] = AssetDatabase.LoadAssetAtPath<T>(path);
            }

            return instances;
        }
    }
}