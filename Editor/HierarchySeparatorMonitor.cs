using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utilities
{
    #if UNITY_EDITOR
    [UnityEditor.InitializeOnLoadAttribute]
    public static class HierarchySeparatorMonitor
    {
        const float SEPARATOR_LABEL_LENGTH = 200.0f;
        const char SEPARATOR_CHARACTER = '-';

        static HierarchySeparatorMonitor()
        {
            UnityEditor.EditorApplication.hierarchyChanged += OnHierarchyChanged;
        }

        static void OnHierarchyChanged()
        {
            HierarchySeparator[] hierarchySeparators = Resources.FindObjectsOfTypeAll<HierarchySeparator>();

            for (int i = 0; i < hierarchySeparators.Length; i++)
            {
                FormatSeparatorName(hierarchySeparators[i]);
            }
        }

        private static void FormatSeparatorName(HierarchySeparator separatorComponent)
        {
            if (separatorComponent.name != separatorComponent.PreviousName)
            {
                float separatorCharacterLength = GetLabelLength(new string(SEPARATOR_CHARACTER, 20)) / 20.0f;

                string trimmedName = separatorComponent.name.Trim(SEPARATOR_CHARACTER, ' ');
                float trimmedNameLabelLength = GetLabelLength(trimmedName);
                float saceToFill = Mathf.Max(0.0f, SEPARATOR_LABEL_LENGTH - trimmedNameLabelLength);
                int charactersToAdd = Mathf.RoundToInt(saceToFill / separatorCharacterLength);

                int prefixLength = charactersToAdd / 2;
                int suffixLength = charactersToAdd - prefixLength;
                trimmedName = $"{new String(SEPARATOR_CHARACTER, prefixLength)} {trimmedName} {new String(SEPARATOR_CHARACTER, suffixLength)}";
                separatorComponent.name = trimmedName;
                separatorComponent.PreviousName = trimmedName;
                UnityEditor.EditorUtility.SetDirty(separatorComponent.gameObject);
            }
        }

        private static float GetLabelLength(string text)
        {
            return UnityEditor.EditorStyles.label.CalcSize(new GUIContent(text)).x;
        }
    }
    #endif
}