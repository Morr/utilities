﻿using UnityEngine;
using System.IO;

namespace Assets.Editor
{
    [UnityEditor.AssetImporters.ScriptedImporter(1, "lua")]
    class LuaImporter : UnityEditor.AssetImporters.ScriptedImporter
    {
        public override void OnImportAsset(UnityEditor.AssetImporters.AssetImportContext context)
        {
            TextAsset asset = new TextAsset(File.ReadAllText(context.assetPath));
            context.AddObjectToAsset("Text", asset);
            context.SetMainObject(asset);
        }
    }
}