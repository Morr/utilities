using System;
using UnityEditor;
using UnityEditor.PackageManager.Requests;
using UnityEditor.PackageManager;
using UnityEngine;
using System.Collections.Generic;

namespace Utilities
{
    static class PackageUpdater
    {
        private const string UTILITIES_GIT_ADDRESS = "http://gitlab.com/Morr/utilities.git";
        private static AddRequest Request { get; set; }

        [MenuItem("Tools/Utilities/Update Package")]
        private static void UpdatePackage()
        {
            Request = Client.Add(UTILITIES_GIT_ADDRESS);
            EditorApplication.update += Progress;
        }

        private static void Progress()
        {
            if (Request.IsCompleted == true)
            {
                if (Request.Status == StatusCode.Success)
                {
                    Debug.Log("Installed: " + Request.Result.packageId);
                }
                else if (Request.Status >= StatusCode.Failure)
                {
                    Debug.Log(Request.Error.message);
                }

                EditorApplication.update -= Progress;
            }
        }
    }
}