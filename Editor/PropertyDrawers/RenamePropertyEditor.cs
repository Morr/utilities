﻿using UnityEngine;
using UnityEditor;

namespace Utilities
{
    [CustomPropertyDrawer(typeof(RenamePropertyAttribute))]
    public class RenamePropertyAttributeEditor : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.PropertyField(position, property, new GUIContent((attribute as RenamePropertyAttribute).NewName));
        }
    }
}