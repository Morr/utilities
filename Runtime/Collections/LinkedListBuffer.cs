using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utilities
{
    public class LinkedListBuffer<T>
    {
        public System.Action<T> OnElementAdded { get; set; } = delegate { };
        public System.Action OnElementRemoved { get; set; } = delegate { };

        public LinkedList<T> Elements { get; set; }
        public LinkedListNode<T> Last => Elements.Last;
        public int Count => Elements.Count;
        private int MaxCount { get; set; }

        public LinkedListBuffer(int maxCount)
        {
            MaxCount = maxCount;
            Elements = new LinkedList<T>();
        }

        public void AddLast(T element)
        {
            Elements.AddLast(element);
            OnElementAdded(element);

            if (Elements.Count > MaxCount)
            {
                Elements.RemoveFirst();
                OnElementRemoved();
            }
        }

        public void Clear()
        {
            Elements.Clear();
        }
    }
}

