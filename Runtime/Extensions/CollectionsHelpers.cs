using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utilities
{
    public static class CollectionsHelpers
    {
        public static int GetLoopedIndex<T>(this IList<T> collection, int index)
        {
            index = ((index % collection.Count) + collection.Count) % collection.Count;
            return index;
        }

        public static void Rewind<T>(this List<T> list, int firstIndex)
        {
            List<T> range = list.GetRange(0, firstIndex);
            list.RemoveRange(0, firstIndex);
            list.AddRange(range);
        }

        public static T GetLastElement<T>(this IList<T> list)
        {
            return list[list.Count - 1];
        }
    }
}

