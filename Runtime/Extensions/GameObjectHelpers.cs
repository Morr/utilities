using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utilities
{
    public static class GameObjectHelpers
    {
        public static void SetTransformsParent(this IList<GameObject> gameObjects, Transform parent)
        {
            for (int i = 0; i < gameObjects.Count; i++)
            {
                gameObjects[i].transform.SetParent(parent);
            }
        }

        public static void DestroyGameObjects(this IList<GameObject> gameObjects)
        {
            for (int i = 0; i < gameObjects.Count; i++)
            {
                GameObject.Destroy(gameObjects[i]);
            }
        }

        public static T GetOrAddComponent<T>(this GameObject gameObject) where T : Component
        {
            T component = gameObject.GetComponent<T>();

            if (component == null)
            {
                component = gameObject.AddComponent<T>();
            }

            return component;
        }
    }
}
