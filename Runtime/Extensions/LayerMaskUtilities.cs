using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utilities
{
    public static class LayerMaskUtilities
    {
        public static bool Contains(this LayerMask mask, int layer)
        {
            return ((mask & (1 << layer)) != 0);
        }
    }
}
