using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utilities
{
    public static class RectTransformHelpers
    {
        public static bool ContainsRectTransform(this RectTransform container, RectTransform rectTransform)
        {
            bool isInside = true;
            Vector3[] corners = new Vector3[4];
            rectTransform.GetWorldCorners(corners);

            for (int i = 0; i < corners.Length; i++)
            {
                Vector3 localSpacePoint = container.InverseTransformPoint(corners[i]);

                if (container.rect.Contains(localSpacePoint) == false)
                {
                    isInside = false;
                    break;
                }
            }

            return isInside;
        }
    }
}
