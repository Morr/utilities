using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utilities
{
    public static class ResolutionHelpers
    {
        public static bool IsEqualTo(this Resolution resolution, Resolution otherResolution)
        {
            return resolution.width == otherResolution.width
                && resolution.height == otherResolution.height
                && resolution.refreshRate == otherResolution.refreshRate;
        }

        public static Vector2Int ToVector2Int(Resolution resolution)
        {
            return new Vector2Int(resolution.width, resolution.height);
        }

        public static string ToStringRepresentation(Vector2Int resolution)
        {
            return $"{resolution.x} x {resolution.y}";
        }

        public static Vector2Int FindCloasestSupportedResolution(Vector2Int targetResolution)
        {
            Resolution[] supportedResolutions = Screen.resolutions;
            float minDifference = int.MaxValue;
            Vector2Int cloasestResolution = default;

            foreach (var resolutionEntry in supportedResolutions)
            {
                Vector2Int resolution = new Vector2Int(resolutionEntry.width, resolutionEntry.height);
                float difference = Vector2Int.Distance(resolution, targetResolution);

                if (difference < minDifference)
                {
                    minDifference = difference;
                    cloasestResolution = resolution;
                }
            }

            return cloasestResolution;
        }
    }
}