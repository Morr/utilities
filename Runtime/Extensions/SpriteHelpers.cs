using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Utilities
{
    public static class SpriteHelpers
    {
        public static void SwapImage(this Image imageComponent, Texture2D newImage)
        {
            imageComponent.sprite = Sprite.Create(newImage, new Rect(0, 0, newImage.width, newImage.height), imageComponent.sprite.pivot);
        }
    }
}

