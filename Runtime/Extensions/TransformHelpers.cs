using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utilities
{
    public static class TransformHelpers
    {
        public static void SetLocalScaleComponent(this Transform transform, int index, float value)
        {
            Vector3 newScale = transform.localScale;
            newScale[index] = value;
            transform.localScale = newScale;
        }

        public static void ApplyValuesTo(this Transform from, Transform to)
        {
            to.position = from.position;
            to.rotation = from.rotation;
            to.localScale = from.localScale;
        }
    }
}
