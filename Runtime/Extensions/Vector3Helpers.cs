using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utilities
{
    public static class Vector3Helpers
    {
        /// <summary>
        /// Memberwise division of tho vectors
        /// </summary>
        public static Vector3 Divide(this Vector3 vector, Vector3 otherVector)
        {
            return new Vector3(vector.x / otherVector.x, vector.y / otherVector.y, vector.z / otherVector.z);
        }

        /// <summary>
        /// Memberwise Mathf.FloorToInt()
        /// </summary>
        public static Vector3Int FloorToInt(this Vector3 vector)
        {
            return new Vector3Int(Mathf.FloorToInt(vector.x), Mathf.FloorToInt(vector.y), Mathf.FloorToInt(vector.z));
        }

        /// <summary>
        /// Memberwise Mathf.CeilToInt()
        /// </summary>
        public static Vector3Int CeilToInt(this Vector3 vector)
        {
            return new Vector3Int(Mathf.CeilToInt(vector.x), Mathf.CeilToInt(vector.y), Mathf.CeilToInt(vector.z));
        }
    }
}
