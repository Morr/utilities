﻿using UnityEngine;

namespace Utilities
{
    public class HierarchySeparator : MonoBehaviour
    {
        [field: SerializeField, HideInInspector]
        public string PreviousName { get; set; }
    }
}