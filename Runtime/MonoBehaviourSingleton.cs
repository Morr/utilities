﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utilities
{
    public abstract class MonoBehaviourSingleton<T> : MonoBehaviour where T : Component
    {
        public static T Instance { get; private set; }

        protected virtual void Awake()
        {
            if (TryAssignReference() == true)
            {
                InitializeSingleton();
            }
        }

        private bool TryAssignReference()
        {
            if (Instance == null)
            {
                Instance = GetComponent<T>();
                return true;
            }
            else
            {
                DestroyImmediate(gameObject);
                return false;
            }
        }

        protected virtual void InitializeSingleton() { }
    }
}