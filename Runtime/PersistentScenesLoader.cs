using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Utilities
{
    public class PersistentScenesLoader : MonoBehaviourSingleton<PersistentScenesLoader>
    {
        [field: SerializeField]
        private SceneReference[] PersistentScenes { get; set; }

        protected override void InitializeSingleton()
        {
            LoadPersistentScenes();
            DontDestroyOnLoad(gameObject);
        }

        private void LoadPersistentScenes()
        {
            for (int i = 0; i < PersistentScenes.Length; i++)
            {
                SceneManager.LoadScene(PersistentScenes[i], LoadSceneMode.Additive);
            }
        }
    }
}