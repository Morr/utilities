﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utilities
{
    public class RenamePropertyAttribute : PropertyAttribute
    {
        public string NewName { get; private set; }

        public RenamePropertyAttribute(string name)
        {
            NewName = name;
        }
    }
}
